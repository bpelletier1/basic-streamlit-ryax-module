from ryax_gateway.gateway import RyaxGateway, main
import subprocess


class StreamlitGateway(RyaxGateway):
    async def handler(self):
        #await self.send_execution({})
        subprocess.run("/bin/streamlit run ./my-script.py --server.port 5002 --server.enableCORS false" , shell=True , env={"HOME": "/tmp"})


if __name__ == "__main__":
    main(StreamlitGateway)
